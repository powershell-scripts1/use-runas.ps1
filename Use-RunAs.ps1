<#
.Synopsis
    Checks if script is running as admin.
    If $Check wasn't passed to the function,
    it automatically assumes to check and rerun
    script as admin.
.NOTES
    Author  : rabel001@gitlab.com & rabeljr@github.com (least used)
    Date    : 10/16/2019
    Version    : 1.0
    URL     : https://gitlab.com/rabel001
.DESCRIPTION
    Checks if script is running as admin.
    If $Check wasn't passed to the function,
    it automatically assumes to check and rerun
    script as admin.
.PARAMETER Check
    Returns the whether the script is running as admin or not. Does not execute as execute as admin.
.EXAMPLE
    . .\Use-Runas.ps1
    Use this line to import the Use-RunAs function in the script file or just add the function to your script.
.EXAMPLE
    Use-Runas -Check
    Use the -Check flag if you just want to get the current admin state. Does not execute as execute as admin.
.EXAMPLE
   Use-RunAs
   Checks if script is running as admin. If it is not, it relaunches itself as admin.
#>

function Use-RunAs
{
	# Check if script is running as Adminstrator and if not use RunAs
	# Use Check Switch to check if admin
	param(
		[Switch]$Check
	)

	$IsAdmin = ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")
	if ($Check) { return $IsAdmin }
	if ($MyInvocation.ScriptName -ne "")
	{
		if (-not $IsAdmin)
		{
			try
			{
				$arg = "-file `"$($MyInvocation.ScriptName)`"";
				Start-Process "$($PSHOME)\powershell.exe" -Verb Runas -ArgumentList $arg -ErrorAction 'stop';
			}
			catch
			{
				Write-Warning "Error - Failed to restart script with runas";
				break;
			}
			exit; # Quit this session of powershell
		}
	}
	else
	{
		Write-Warning "Error - Script must be saved as a .ps1 file first";
		break;
	}
}

